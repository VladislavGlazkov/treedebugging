﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnvDTE;
using Microsoft.VisualStudio.Debugger.Interop;
using Microsoft.VisualStudio.Shell.Interop;

namespace DebuggerCommunication
{
    public class DebComm
    {
        Debugger debugger;
        DebuggerEvents debuggerEvents;
        DocumentEvents DocumentEvents;

        public delegate void ddd();
        public event ddd Update;
        public event ddd Terminate;
        public DebComm()
        {
            var dte = (DTE)Microsoft.VisualStudio.Shell.Package.GetGlobalService(typeof(DTE));
            debugger = dte.Debugger;
            debuggerEvents = dte.Events.DebuggerEvents;
            DocumentEvents = dte.Events.DocumentEvents;
            DocumentEvents.DocumentSaved += DocumentEvents_DocumentSaved;
            debuggerEvents.OnContextChanged += DebuggerEvents_OnContextChanged; ;
            
            debuggerEvents.OnEnterDesignMode += DebuggerEvents_OnEnterDesignMode;
        }

        private void DebuggerEvents_OnEnterDesignMode(dbgEventReason Reason)
        {
            Terminate();
        }

        

        private void DebuggerEvents_OnContextChanged(Process NewProcess, Program NewProgram, Thread NewThread, StackFrame NewStackFrame)
        {
            Console.WriteLine("dfnj");
            if (NewThread != null)
            {
                Update();

            }
            else
            {
                Terminate();
            }

        }
        
        private void DocumentEvents_DocumentSaved(Document Document)
        {
            Console.WriteLine("dfnj");
        }

        private void DebuggerEvents_OnEnterBreakMode(dbgEventReason Reason, ref dbgExecutionAction ExecutionAction)
        {
            Update();
        }
        public string GetPtr(string name,string typename)
        {
            string ans = debugger.GetExpression(name).Value;
            if (ans == "NULL")
            {
                return "NULL";
            }
            else
            {
                
                return "(("+typename+"*)"+name+")";
            }
        }
        public int[] GetArray(string name)
        {
             
            var hh = Int32.Parse(debugger.GetExpression(name + ".size()").Value);
            int[] ans = new int[hh];
            for (int i = 0; i < hh; i++)
            {
                int hhh = Int32.Parse(debugger.GetExpression(name + "[" + i.ToString() + "]").Value);
                ans[i] = hhh;
            }
            return ans;

        }
        public string GetStr(string name)
        {
            try
            {
                return debugger.GetExpression(name).Value;
            }
            catch
            {
                return "null";
            }
        }
        public int GetNum(string name)
        {
            try
            {
                int hhh = Int32.Parse(debugger.GetExpression(name).Value);
                return hhh;
            }
            catch
            {
                return -1;
            }

        }

    }
}

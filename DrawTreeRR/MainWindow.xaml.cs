﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using TreeData;
namespace DrawTreeRR
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class DrawTreeR : Window,IDrawTree
    {
        public void SetNum(int a)
        {
            this.Title = "Window " + a.ToString();
        }
        public DrawTreeR()
        {
            InitializeComponent();
            
        }
        public void Redraw (TreeData.TreeData data)
        {
            Grid grid = new Grid(); ;
            int n = data.n;
            int levels = 0;
            int tmax = 0;
            for (int i = 0; i < n; i++)
            {
                levels = Math.Max(levels, data.Level[i]);
                tmax = Math.Max(data.Tout[i]+1, tmax);
            }

            for (int i = 0; i < tmax; i++)
            {
                ColumnDefinition cld = new ColumnDefinition();
                cld.Width = new GridLength(1, GridUnitType.Star);
                grid.ColumnDefinitions.Add(cld);
            }
            for (int i = 0; i <= levels; i++)
            {
                RowDefinition rod = new RowDefinition();
                rod.Height = new GridLength(2, GridUnitType.Star);
                grid.RowDefinitions.Add(rod);
            }
            {
                RowDefinition rod = new RowDefinition();
                rod.Height = new GridLength(1, GridUnitType.Star);
                grid.RowDefinitions.Add(rod);
            }
            for (int i = 0; i < tmax; i++)
            {
                Border rectangle = new Border();
                grid.Children.Add(rectangle);
                grid.ShowGridLines = true;
                rectangle.BorderBrush = new SolidColorBrush(Colors.Black);
                rectangle.BorderThickness = new Thickness(10);
                Grid.SetColumn(rectangle,i);
                Grid.SetRow(rectangle, levels + 1);

                TextBlock tbx = new TextBlock();
                tbx.Text = i.ToString();
                
                tbx.VerticalAlignment = VerticalAlignment.Center;
                tbx.HorizontalAlignment = HorizontalAlignment.Center;
                grid.Children.Add(tbx);
                Grid.SetColumn(tbx,i);
                Grid.SetRow(tbx, levels+1);

            }
            for (int i = 0; i < n; i++)
            {
                if (data.Tin[i] >= 0)
                {
                    Border rectangle = new Border();
                    grid.Children.Add(rectangle);
                    grid.ShowGridLines = true;
                    rectangle.BorderBrush = new SolidColorBrush(Colors.Black);
                    rectangle.BorderThickness = new Thickness(10);

                    Grid.SetColumn(rectangle, data.Tin[i]);
                    Grid.SetColumnSpan(rectangle, data.Tout[i] - data.Tin[i] + 1);
                    Grid.SetRow(rectangle, data.Level[i]);
                    TextBlock tbx = new TextBlock();
                    tbx.VerticalAlignment = VerticalAlignment.Center;
                    tbx.HorizontalAlignment = HorizontalAlignment.Center;
                    tbx.Text = "ID: " + i.ToString();
                    foreach (var kval in data.Data[i])
                    {
                        tbx.Text += '\n' + kval.Key + ':' + kval.Value;
                    }
                    rectangle.Background =new SolidColorBrush( data.Cls[i]);
                    Grid.SetColumn(tbx, data.Tin[i]);
                    Grid.SetColumnSpan(tbx, data.Tout[i] - data.Tin[i] + 1);
                    Grid.SetRow(tbx, data.Level[i]);
                    grid.Children.Add(tbx);
                }
            }
            this.Content = grid;
        }
    }
}

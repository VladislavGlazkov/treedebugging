﻿
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using Xceed.Wpf.Toolkit;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System;
using TreeData;
namespace FirstDebugging
{
    /// <summary>
    /// Interaction logic for ToolWindow1Control.
    /// </summary>
    /// 
   
    [Serializable]
    public class WinData
    {
        public List<TreeData.KeyValuePair<bool, List<string>>> mainTree = new List<TreeData.KeyValuePair<bool, List<string>>>();
        public List<TreeData.KeyValuePair<bool, List<string>>> edges = new List<TreeData.KeyValuePair<bool, List<string>>>();
        public List<TreeData.KeyValuePair<string,List<string>>> data= new List<TreeData.KeyValuePair<string, List<string>>>();
        public List<TreeData.KeyValuePair<System.Windows.Media.Color, List<string>>> coloring= new List<TreeData.KeyValuePair<System.Windows.Media.Color, List<string>>>();
        public string tbx1, tbx2;
        public bool IsStruct = false;
        public bool IsRView = false;
        public int IsShifted = 0;
        public WinData() { }
        public WinData (WinDataRaw gdata)
        {
            foreach (var h in gdata.mainTree)
            {
                mainTree.Add(h.SaveData());
            }
            foreach (var h in gdata.edges)
            {
                List<string> lst = new List<string>();
                foreach (var ss in h.Value)
                {
                    lst.Add(ss.Text);

                }
                edges.Add(new TreeData.KeyValuePair<bool, List<string>>(h.Key.SelectedIndex==1, lst));
                
            }
            
            foreach (var h in gdata.data)
            {
                List<string> lst = new List<string>();
                for (int i = 1; i < h.Count; i++)
                {
                    lst.Add(h[i].Text);
                }
                data.Add(new TreeData.KeyValuePair<string, List<string>>(h[0].Text, lst));
            }
            foreach (var h in gdata.coloring)
            {
                List<string> lst = new List<string>();
                foreach (var ss in h.Value)
                {
                    lst.Add(ss.Text);

                }
                coloring.Add(new TreeData.KeyValuePair<System.Windows.Media.Color, List<string>>((System.Windows.Media.Color)h.Key.SelectedColor, lst));
            }
            tbx1 = gdata.tbx1.Text;
            tbx2 = gdata.tbx2.Text;
            IsShifted = gdata.IsShifted.SelectedIndex;
            IsRView = gdata.IsRView.SelectedIndex==1;
            IsStruct = (bool)gdata.cx1.IsChecked ? false : true;
            
        }
    }
    
    public class WinDataRaw
    {
        public List<Sabox> mainTree = new List<Sabox>();
        public List<TreeData.KeyValuePair<ComboBox, List<TextBox>>> edges = new List<TreeData.KeyValuePair<ComboBox, List<TextBox>>>();
        public List<List<TextBox>> data=new List<List<TextBox>>();
        public List<TreeData.KeyValuePair<ColorPicker, List<TextBox>>> coloring= new List<TreeData.KeyValuePair<ColorPicker, List<TextBox>>>();
        public TextBox tbx1= new TextBox(), tbx2= new TextBox();
        public RadioButton cx1= new RadioButton(), cx2= new RadioButton();
        public ComboBox IsRView= new ComboBox();
        public ComboBox IsShifted= new ComboBox();
        public List<Button> buttons = new List<Button>();
    }
 
    public partial class ToolWindow1Control : UserControl
    {
        public List<WinData> ExternalData=new List<WinData>();
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolWindow1Control"/> class.
        /// </summary>
        /// 



        int curwnd = 0;
        public List<WinDataRaw> gdata = new List<WinDataRaw>();
        public void LoadData(List<WinData> datas)
        {
            TabControl tco = windows;
            windows.Items.Clear();
            gdata = new List<WinDataRaw>();
            int index = -1;
            foreach (var h in datas)
            {
                index++;
                curwnd = index;
                TabItem titem = new TabItem();
                ((TabItem)titem).Header = new Label();
                ((Label)(titem.Header)).Content = "Window" + (index+1).ToString();
                titem.Content = getStandard();
                gdata[index].tbx1.Text = datas[index].tbx1;
                gdata[index].tbx2.Text = datas[index].tbx2;
                if (datas[index].IsStruct)
                {
                    gdata[index].cx2.IsChecked = true;
                }
                else
                {
                    gdata[index].cx1.IsChecked = true;
                }
                gdata[index].IsRView.SelectedIndex = datas[index].IsRView==true?1:0;
                gdata[index].IsShifted.SelectedIndex = datas[index].IsShifted;
                for (int j=0;j<datas[index].mainTree.Count;j++)
                {
                    Button btn = gdata[index].buttons[0];
                    Btn1_Click(btn, null);
                    
                    gdata[index].mainTree[j].LoadData(datas[index].mainTree[j]);
                    
                }
                for (int j = 0; j < datas[index].data.Count; j++)
                {
                    Button btn = gdata[index].buttons[2];
                    Btn3_Click(btn, null);


                    gdata[index].data[j][0].Text = datas[index].data[j].Key;
                    for (int k = 0; k < datas[index].data[j].Value.Count; k++)
                    {

                        gdata[index].data[j][k+1].Text = datas[index].data[j].Value[k];
                    }
                }
                for (int j = 0; j < datas[index].coloring.Count; j++)
                {
                    Button btn = gdata[index].buttons[3];
                    Btn_Click4(btn, null);

                    gdata[index].coloring[j].Key.SelectedColor = datas[index].coloring[j].Key;
                    for (int k = 0; k < datas[index].coloring[j].Value.Count; k++)
                    {

                        gdata[index].coloring[j].Value[k].Text = datas[index].coloring[j].Value[k];
                    }

                }
                windows.Items.Add(titem);
            }
            TabItem tit = new TabItem();
            tit.Header = "+";
            windows.Items.Add(tit);



            int i = 0;
            foreach (TabItem h in tco.Items)
            {
                if (tco.Items[tco.Items.Count - 1] != h)
                {
                    h.Header = new Label();
                    ((Label)h.Header).Content = "Window" + (++i).ToString();
                    ((Label)h.Header).MouseDoubleClick += Uel_MouseDoubleClick;
                }
            }
        }
        public ToolWindow1Control(List<WinData> data)
        {
           
            ExternalData = data;
            InitializeComponent();
            saver.Click += Saver_Click;
            opener.Click += Opener_Click;
            windows.SelectionChanged += Windows_SelectionChanged;
            
            this.Unloaded += ToolWindow1Control_Unloaded;
            {
                TabItem it = new TabItem();
                it.Header = new Label();
                ((Label)it.Header).Content = "Windows1";
                windows.Items.Add(it);
                it = new TabItem();
                it.Header = new Label();
                ((Label)it.Header).Content = "+";
                windows.Items.Add(it);

            }
            foreach (TabItem h in windows.Items)
            {
                if ((string)((Label)h.Header).Content != "+")
                    h.Content = getStandard();
                ((Label)((TabItem)h).Header).MouseDoubleClick += Uel_MouseDoubleClick;
            }

        }

        private void Opener_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog ovd= new Microsoft.Win32.OpenFileDialog();
            if (ovd.ShowDialog() == true)
            {
                string fname = ovd.FileName;
                XmlSerializer xse = new XmlSerializer(typeof(List<WinData>));
                FileStream fs = new FileStream(fname, FileMode.Open);
                
                /*string str = System.Text.Encoding.ASCII.GetString(bb);
                List<WinData> h = System.Text.Json.JsonSerializer.Deserialize<List<WinData> >(str);*/
                List<WinData> h=(List<WinData>)xse.Deserialize(fs);
                LoadData(h);
            }
        }

        private void Saver_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog svd = new Microsoft.Win32.SaveFileDialog();
            
            if (svd.ShowDialog() == true)
            {
                string fname = svd.FileName;
               
                XmlSerializer xse = new XmlSerializer(typeof(List<WinData>));
                FileStream fs = new FileStream(fname, FileMode.Create);
                List<WinData> lwd = new List<WinData>();
                foreach(var h in gdata)
                {
                    lwd.Add(new WinData(h));
                }
                /*string sss = System.Text.Json.JsonSerializer.Serialize<List<WinData> >(lwd);
                byte[] bts = System.Text.Encoding.ASCII.GetBytes(sss);
                fs.Write(bts, 0, bts.Length);*/
                xse.Serialize(fs, lwd);
                
            }
        }

        public ToolWindow1Control()
        {

            InitializeComponent();
            windows.SelectionChanged += Windows_SelectionChanged;
            this.Unloaded += ToolWindow1Control_Unloaded;
            {
                TabItem it = new TabItem();
                it.Header = new Label();
                ((Label)it.Header).Content = "Windows1";
                windows.Items.Add(it);
                it = new TabItem();
                it.Header = new Label();
                ((Label)it.Header).Content = "+";
                windows.Items.Add(it);

            }
            foreach (TabItem h in windows.Items)
            {
                if ((string)((Label)h.Header).Content!="+")
                    h.Content = getStandard();
                ((Label)((TabItem)h).Header).MouseDoubleClick += Uel_MouseDoubleClick;
            }
            
        }

        private void ToolWindow1Control_Unloaded(object sender, RoutedEventArgs e)
        {
            ExternalData.Clear();
            foreach (var h in gdata)
            {
                ExternalData.Add(new WinData(h));
            }   
        }

        Grid getStandard()
        {
            gdata.Add(new WinDataRaw());
            Grid result = new Grid();
            result.ColumnDefinitions.Add(new ColumnDefinition());
            RowDefinition rde = new RowDefinition();
            rde.Height = new GridLength(100);
            result.RowDefinitions.Add(rde);
            result.RowDefinitions.Add(new RowDefinition());

            Grid temp = new Grid();
            temp.ColumnDefinitions.Add(new ColumnDefinition());
            temp.ColumnDefinitions.Add(new ColumnDefinition());
            temp.ColumnDefinitions.Add(new ColumnDefinition());
            temp.RowDefinitions.Add(new RowDefinition());
            temp.RowDefinitions.Add(new RowDefinition());
            Grid.SetRow(temp, 0);

            RadioButton radioButton = new RadioButton();
            radioButton.Content = "Standard";
            radioButton.IsChecked = true;
            gdata[curwnd].cx1 = radioButton;
            Grid.SetRow(radioButton, 0);
            temp.Children.Add(radioButton);
            radioButton = new RadioButton();
            radioButton.Content = "Struct";
            gdata[curwnd].cx2 = radioButton;
            Grid.SetRow(radioButton, 1);
            temp.Children.Add(radioButton);

            ComboBox comboBox = new ComboBox();
            ComboBoxItem cbi = new ComboBoxItem();
            cbi.Content = "Standard View";
            comboBox.Items.Add(cbi);
            cbi = new ComboBoxItem();
            cbi.Content = "Rectangle View";
            comboBox.SelectedIndex = 0;
            comboBox.Items.Add(cbi);
            gdata[curwnd].IsRView = comboBox;
            Grid.SetColumn(comboBox, 2);
            temp.Children.Add(comboBox);


            ComboBox check = new ComboBox();
            ComboBoxItem cit= new ComboBoxItem();
            cit.Content = "Standard";
            check.Items.Add(cit);

            cit = new ComboBoxItem();
            cit.Content = "Shifted";
            check.Items.Add(cit);
            cit = new ComboBoxItem();
            cit.Content = "Treap";
            check.SelectedIndex = 0;
            
            
            check.Items.Add(cit);
            gdata[curwnd].IsShifted = check;
            Grid.SetColumn(check,2);
            Grid.SetRow(check, 1);
            temp.Children.Add(check);
            // RadioButton_CheckedStan(radioButton, null);
            {
                TextBox tbx = new TextBox();
                Grid.SetColumn(tbx, 1);
                Grid.SetRow(tbx, 0);
                temp.Children.Add(tbx);
                gdata[curwnd].tbx1 = tbx;
                tbx = new TextBox();
                Grid.SetColumn(tbx, 1);
                Grid.SetRow(tbx, 1);
                temp.Children.Add(tbx);
                gdata[curwnd].tbx2 = tbx;
            }
            result.Children.Add(temp);

            TabControl tabControl = new TabControl();
            tabControl.TabStripPlacement = Dock.Left;
            TabItem item = new TabItem();
            item.Header = "Tree Edges";

            item.Content = new ListBox();
            {
                ListBox cnt = (ListBox)item.Content;
                Button btn = new Button();
                btn.Click += Btn1_Click; ;
                btn.Content = "Add";
                gdata[curwnd].buttons.Add(btn);
                ListBoxItem litem = new ListBoxItem();
                litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                litem.Content = btn;

                cnt.Items.Add(litem);


            }
            tabControl.Items.Add(item);
            
            item = new TabItem();
            item.Header = "Data Picker";
            item.Content = new ListBox();
            {
                ListBox cnt = (ListBox)item.Content;
                Button btn = new Button();
                btn.Click += Btn3_Click; ;
                btn.Content = "Add";
                gdata[curwnd].buttons.Add(btn);

                ListBoxItem litem = new ListBoxItem();
                litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                litem.Content = btn;

                cnt.Items.Add(litem);


            }
            tabControl.Items.Add(item);
            item = new TabItem();
            item.Header = "Color Vertices";

            item.Content = new ListBox();
            {
                ListBox cnt = (ListBox)item.Content;
                Button btn = new Button();
                btn.Click += Btn_Click4; ; ;
                btn.Content = "Add";
                gdata[curwnd].buttons.Add(btn);

                ListBoxItem litem = new ListBoxItem();
                litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                litem.Content = btn;

                cnt.Items.Add(litem);


            }
            tabControl.Items.Add(item);


            Grid.SetRow(tabControl, 1);
            result.Children.Add(tabControl);



            return result;
        }

      

        private void Btn_Click4(object sender, RoutedEventArgs e)
        {
            ListBox lbx = (ListBox)((ListBoxItem)((Control)sender).Parent).Parent;
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < 2; i++)
            {
                ColumnDefinition cdf = new ColumnDefinition();
                cdf.Width = new GridLength(1, GridUnitType.Star);
                grid.ColumnDefinitions.Add(cdf);
            }

            TextBox tbx = new TextBox();
            TextBox ttx1, ttx2;
            ttx1 = tbx;
            Grid.SetColumn(tbx, 0);
            grid.Children.Add(tbx);
            
            ColorPicker colorPicker = new ColorPicker();
            colorPicker.SelectedColor = System.Windows.Media.Colors.White;
            Grid.SetColumn(colorPicker, 2);
            grid.Children.Add(colorPicker);
            gdata[curwnd].coloring.Add(new TreeData.KeyValuePair<ColorPicker, List<TextBox>>(colorPicker,new List<TextBox> { ttx1 }));
            ListBoxItem litem = new ListBoxItem();
            litem.Content = grid;
            litem.MouseDoubleClick += Litem_MouseDoubleClick4; ;
            litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            lbx.Items.Insert(lbx.Items.Count - 1, litem);
        }

        

        private void Litem_MouseDoubleClick4(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem litem = (ListBoxItem)sender;
            ListBox par = (ListBox)litem.Parent;
            gdata[curwnd].coloring.RemoveAt(par.Items.IndexOf(sender));
            par.Items.Remove(sender);
        }

       
      

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            ListBox lbx = (ListBox)((ListBoxItem)((Control)sender).Parent).Parent;
            Grid grid = new Grid();
            grid.RowDefinitions.Add(new RowDefinition());
            for (int i = 0; i < 2; i++)
            {
                ColumnDefinition cdf = new ColumnDefinition();
                cdf.Width = new GridLength(1, GridUnitType.Star);
                grid.ColumnDefinitions.Add(cdf);
            }

            TextBox tbx = new TextBox();
            TextBox ttx1,ttx2;
            ttx1 = tbx;
            Grid.SetColumn(tbx, 0);
            grid.Children.Add(tbx);
            tbx = new TextBox();
            ttx2 = tbx;
            Grid.SetColumn(tbx, 1);
            grid.Children.Add(tbx);
            gdata[curwnd].data.Add(new List<TextBox> { ttx1,ttx2 });
            ListBoxItem litem = new ListBoxItem();
            litem.Content = grid;
            litem.MouseDoubleClick += Litem_MouseDoubleClick3;
            litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            lbx.Items.Insert(lbx.Items.Count - 1, litem);
        }
        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            ListBox lbx = (ListBox)((ListBoxItem)((Control)sender).Parent).Parent;
            Sabox sbx = new Sabox();
            gdata[curwnd].mainTree.Add(sbx);
            ListBoxItem litem = new ListBoxItem();
            litem.Content = sbx;
            litem.MouseDoubleClick += Litem_MouseDoubleClick1;
            litem.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            lbx.Items.Insert(lbx.Items.Count - 1, litem);
        }

        
        private void Cbx_SelectionChanged3(object sender, SelectionChangedEventArgs e)
        {
            Grid grid = (Grid)((ComboBox)sender).Parent;

            if (grid != null)
            {
                ComboBox comboBox = (ComboBox)sender;
                List<Control> fre = new List<Control>();
                foreach (Control ch in grid.Children)
                {
                    if (Grid.GetColumn(ch) != 0)
                    {
                        fre.Add(ch);

                    }
                }
                foreach (Control re in fre)
                {
                    grid.Children.Remove(re);
                }
                while (grid.ColumnDefinitions.Count > 1)
                {
                    grid.ColumnDefinitions.RemoveAt(grid.ColumnDefinitions.Count - 1);
                }
                if (comboBox.SelectedIndex == 0)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        ColumnDefinition cdc = new ColumnDefinition();
                        grid.ColumnDefinitions.Add(cdc);
                        cdc.Width = new GridLength(1, GridUnitType.Star);
                        TextBox tbx = new TextBox();
                        grid.Children.Add(tbx);
                        Grid.SetColumn(tbx, i);

                    }
                }
                else
                {
                    for (int i = 1; i <= 4; i++)
                    {
                        ColumnDefinition cdc = new ColumnDefinition();
                        grid.ColumnDefinitions.Add(cdc);

                        cdc.Width = new GridLength(1, GridUnitType.Star);
                        TextBox tbx = new TextBox();
                        grid.Children.Add(tbx);
                        Grid.SetColumn(tbx, i);

                    }
                }
            }
        }

        private void Litem_MouseDoubleClick1(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem litem = (ListBoxItem)sender;
            ListBox par = (ListBox)litem.Parent;
            gdata[curwnd].mainTree.RemoveAt(par.Items.IndexOf(sender));
            par.Items.Remove(sender);
        }
        private void Litem_MouseDoubleClick3(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem litem = (ListBoxItem)sender;
            ListBox par = (ListBox)litem.Parent;
            gdata[curwnd].data.RemoveAt(par.Items.IndexOf(sender));
            par.Items.Remove(sender);
        }

        private void Uel_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TabControl tco = (TabControl)((TabItem)((Control)sender).Parent).Parent;
            object forremove = ((Control)sender).Parent;
            tco.SelectedIndex = 0;
            curwnd = 0;
            gdata.RemoveAt(tco.Items.IndexOf(forremove));
            tco.Items.Remove(forremove);
            
            int i = 0;
            foreach (TabItem h in tco.Items)
            {
                if (tco.Items[tco.Items.Count - 1] != h)
                {
                    h.Header = new Label();
                    ((Label)h.Header).Content = "Window" + (++i).ToString();
                    ((Label)h.Header).MouseDoubleClick += Uel_MouseDoubleClick;
                }
            }
        }

        private void Windows_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tco = (TabControl)((Control)sender);
            if (tco.Items.Count == 0)
                return;
            curwnd = tco.SelectedIndex;
            if (tco.Items[tco.Items.Count - 1] == tco.SelectedItem)
            {
                TabItem tin = new TabItem();
                tin.Header = "+";

                tco.Items.Add(tin);
                ((TabItem)((TabControl)sender).SelectedItem).Header = new Label();
                ((Label)(((TabItem)((TabControl)sender).SelectedItem).Header)).Content = "Window" + (tco.Items.Count - 1).ToString();
                ((TabItem)((TabControl)sender).SelectedItem).Content = getStandard();
            }
            int i = 0;
            foreach (TabItem h in tco.Items)
            {
                if (tco.Items[tco.Items.Count - 1] != h)
                {
                    h.Header = new Label();
                    ((Label)h.Header).Content = "Window" + (++i).ToString();
                    ((Label)h.Header).MouseDoubleClick += Uel_MouseDoubleClick;
                }
            }

        }


    }


}

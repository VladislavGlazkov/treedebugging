﻿using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using Xceed.Wpf.Toolkit;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using System;
using TreeData;
namespace FirstDebugging
{
    public class Sabox:UserControl
    {
        
        List<TextBox> lst = new List<TextBox>();
        ComboBox cbx = new ComboBox();
        Grid grid = new Grid();
        public Sabox()
        {
            
            this.Content = grid;
            
            LoadData(new TreeData.KeyValuePair<bool, List<string>> (false,new List<string> { "" }));
        }
        public void LoadData(TreeData.KeyValuePair<bool,List<string>> data)
        {
            lst = new List<TextBox>();
            grid.Children.Clear();
            int iii = 0;
            grid.ColumnDefinitions.Clear();
           for (int i = 0; i < data.Value.Count + 1; i++)
            {

                ColumnDefinition cdf = new ColumnDefinition();
                cdf.Width = new GridLength(1, GridUnitType.Star);
                grid.ColumnDefinitions.Add(cdf);
            }
            foreach (var h in data.Value)
            {
                
                TextBox tbs = new TextBox();
                tbs.Text = h;
                grid.Children.Add(tbs);
                Grid.SetColumn(tbs, ++iii);
                lst.Add(tbs);
            }
            cbx = new ComboBox();
            ComboBoxItem cbi = new ComboBoxItem();
            cbi.Content = "Single";
            cbx.Items.Add(cbi);
            cbi = new ComboBoxItem();
            cbi.Content = "Array";
            cbx.Items.Add(cbi);
            grid.Children.Add(cbx);
            Grid.SetColumn(cbx, 0);
            if (data.Key == true)
            {
                cbx.SelectedIndex = 1;
            }
            else
            {
                cbx.SelectedIndex = 0;
            }
            cbx.SelectionChanged += Cbx_SelectionChanged;


        }

        private void Cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var h in lst)
            {
                grid.Children.Remove(h);

            }
            lst.Clear();
            if (cbx.SelectedIndex == 0)
            {
                grid.ColumnDefinitions.Clear();
                for (int i = 0; i <2; i++)
                {

                    ColumnDefinition cdf = new ColumnDefinition();
                    cdf.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(cdf);
                }
                for (int i = 0; i < 1; i++)
                {

                    TextBox tbx = new TextBox();
                    tbx.Text = "";
                    lst.Add(tbx);
                    grid.Children.Add(tbx);
                    Grid.SetColumn(tbx, i+1);

                }
            }
            else
            {
                grid.ColumnDefinitions.Clear();
                for (int i = 0; i <5; i++)
                {

                    ColumnDefinition cdf = new ColumnDefinition();
                    cdf.Width = new GridLength(1, GridUnitType.Star);
                    grid.ColumnDefinitions.Add(cdf);
                }
                for (int i = 0; i < 4; i++)
                {

                    TextBox tbx = new TextBox();
                    tbx.Text = "";
                    lst.Add(tbx);
                    grid.Children.Add(tbx);
                    Grid.SetColumn(tbx, i+1);
                }
            }
        }
        public TreeData.KeyValuePair<bool,List<string>> SaveData()
        {
            bool fst = cbx.SelectedIndex == 1 ? true : false;
            List<string> ll = new List<string>();
            foreach (var h in lst)
            {
                ll.Add(h.Text);
            }
            return new TreeData.KeyValuePair<bool, List<string>>(fst, ll);
        }
    }
}

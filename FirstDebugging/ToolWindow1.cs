﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using DrawTreeCC;
using DrawTreeRR;
using TreeData;
using DebuggerCommunication;
namespace FirstDebugging
{
    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    /// <para>
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </para>
    /// </remarks>
    [Guid("44244051-b99e-479d-be7a-8184603f107c")]
    public class ToolWindow1 : ToolWindowPane
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolWindow1"/> class.
        /// </summary>
        /// 

        List<WinData> winDatas = new List<WinData>();
        DebComm dcom;
        public ToolWindow1() : base(null)
        {
            this.Caption = "ToolWindow1";

            // This is the user control hosted by the tool window; Note that, even if this class implements IDisposable,
            // we are not calling Dispose on this object. This is because ToolWindowPane calls Dispose on
            // the object returned by the Content property.
            this.Content = new ToolWindow1Control(winDatas);

            dcom = new DebComm();
            dcom.Update += Dcom_Update;
            dcom.Terminate += Dcom_Terminate;
        }

        private void Dcom_Terminate()
        {
            foreach (var h in drawTrees)
            {
                h.Close();

            }
            drawTrees = new List<IDrawTree>();
        }

        List<IDrawTree> drawTrees = new List<IDrawTree>(); 
        private void Dcom_Update()
        {
            for (int i = 0; i < winDatas.Count; i++)
            {
                if (drawTrees.Count == i)
                {
                    IDrawTree idtt;
                    if (winDatas[i].IsRView)
                    {
                        idtt = new DrawTreeR();
                        
                    }
                    else
                    {
                        idtt = new DrawTreeC();

                    }
                    idtt.Show();
                    idtt.SetNum(i+1);
                    drawTrees.Add(idtt);
                }
                IDrawTree idt = drawTrees[i];

                TreeDataFactory tdf;
                if (winDatas[i].IsStruct)
                {
                    tdf = new TreeDataFactoryStruct(dcom);

                }
                else
                {
                    tdf = new TreeDataFactoryMain(dcom);
                }
                tdf.AddMainTree(winDatas[i].mainTree, winDatas[i].tbx1, winDatas[i].tbx2, winDatas[i].IsShifted) ;
                foreach (var h in winDatas[i].edges)
                {
                    tdf.AddEdges(h.Key, h.Value, System.Windows.Media.Colors.Black);
                }
                foreach (var h in winDatas[i].data)
                {
                    tdf.AddInfo(h.Key, h.Value);
                }
                foreach (var h in winDatas[i].coloring)
                {
                    tdf.AddColor(h.Value, h.Key);
                }
                idt.Redraw(tdf.getData());

            }
        }
    }
}

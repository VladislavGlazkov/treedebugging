﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DebuggerCommunication;
using System.Windows.Media;
namespace TreeData
{
    
    [Serializable]
    public class KeyValuePair<T1, T2>
    {
        public T1 Key;
        public T2 Value;
        public KeyValuePair(T1 t1, T2 t2)
        {
            Key = t1;
            Value = t2;
        }
        public KeyValuePair() { }
    }
    public interface IDrawTree
    {
        void Redraw(TreeData tree);
        void Show();
        void Close();
        void SetNum(int num);
    }

    public class TreeData
    {
        public int n;
        public List<List<int>> G = new List<List<int>>();
        public List<List<int>> PG = new List<List<int>>();

        public List<List<KeyValuePair<string, string>>> Data = new List<List<KeyValuePair<string, string>>>();
        public List<List<int>> Edges;
        public List<Color> Cls = new List<Color>();
        protected List<int> vis = new List<int>();
        public List<int> Level = new List<int>(), Tin = new List<int>(), Tout = new List<int>();
        public int maxdep;
        protected int ctr = 0;
        public  int isshifted = 1;
        public int root = 0;
    }
    public interface TreeDataFactory
    {
        TreeDataMain getData();
        void AddMainTree(List<KeyValuePair<bool, List<string>>> data,string root, string n,int type);
        void AddInfo(string label, List<string> info);
        void AddEdges(bool isarray, List<string> data, Color clr);
        void AddColor(List<string> condition, Color color);
    }
    public class TreeDataFactoryMain:TreeDataFactory
    {
        string[] seps = new string[1] { "$arr$" };
        TreeDataMain tdata= new TreeDataMain();
        DebComm dcom;

        public TreeDataFactoryMain(DebComm dcom)
        {
            this.dcom = dcom;
        }
        public TreeDataMain getData()
        {

            return tdata;
        }
        
        public void AddMainTree(List<KeyValuePair<bool, List<string>> > data,string root,string n,int type)
        {
            tdata.isshifted = type;
            int sz = dcom.GetNum(n);
            tdata.root = dcom.GetNum(root);

            tdata.n = sz;
            for (int i = 0; i < sz; i++)
            {
                tdata.Cls.Add(Colors.White);
                tdata.Data.Add(new List<KeyValuePair<string, string>>());
            }
           
            for (int i = 0; i < sz; i++)
            {
                
                tdata.G.Add(new List<int>());
                tdata.PG.Add(new List<int>());
                bool ok = false;

                foreach (var par in data)
                {
                    if (!ok&&type==2)
                    {
                        if (par.Key == true)
                        {
                            string sst = par.Value[0].Replace("$id$", i.ToString()).Split(seps,StringSplitOptions.None)[0];
                            int ssz = dcom.GetNum(sst+ ".size()");
                            for (int j = dcom.GetNum(par.Value[0].Replace("$id$",i.ToString())); (dcom.GetStr(par.Value[1].Replace("$id$", i.ToString()).Replace("$pos$",j.ToString()))=="true"); j= dcom.GetNum(par.Value[2].Replace("$id$", i.ToString()).Replace("$pos$", j.ToString())))
                            {
                                string ssst = par.Value[3].Replace("$id$", i.ToString()).Replace("$arr$","").Replace("$pos$", j.ToString());
                                tdata.PG[i].Add(dcom.GetNum(par.Value[3].Replace("$id$", i.ToString()).Replace("$pos$", j.ToString())));
                            }

                        }
                        else
                        {
                            string sst = par.Value[0].Replace("$id$", i.ToString());
                            int cur = dcom.GetNum(sst);
                            if (cur >= 0 && cur < sz)
                            {
                                tdata.PG[i].Add(cur);
                            }
                        }
                    }
                    else
                    {
                        if (par.Key == true)
                        {
                            int ssz = dcom.GetNum(par.Value[0].Replace("$id$",i.ToString()).Split(seps,StringSplitOptions.None)[0] + ".size()");
                            for (int j = dcom.GetNum(par.Value[0].Replace("$id$", i.ToString())); (dcom.GetStr(par.Value[1].Replace("$id$", i.ToString()).Replace("$pos$", j.ToString())) == "true"); j = dcom.GetNum(par.Value[2].Replace("$id$", i.ToString()).Replace("$pos$", j.ToString())))
                            {
                                tdata.G[i].Add(dcom.GetNum(par.Value[3].Replace("$id$",i.ToString()).Replace("$pos$",j.ToString()).Replace("$arr$","")));
                            }

                        }
                        else
                        {
                            int cur = dcom.GetNum(par.Value[0].Replace("$id$",i.ToString()).Replace("$arr$", ""));
                            if (cur >= 0 && cur < sz)
                            {
                                tdata.G[i].Add(cur);
                            }
                        }
                    }
                    ok = true;

                }

            }
            tdata.Update();
        }
        public void AddInfo(string label,List<string>  info)
        {
            int n = tdata.n;
            for (int i = 0; i < n; i++)
            {
                tdata.Data[i].Add(new KeyValuePair<string, string>(label, dcom.GetStr(info[0].Replace("$id$",i.ToString()).Replace("$arr$",""))));
            }
        }
        public void AddColor(List<string> condition,Color color)
        {
            
            for (int i = 0; i < tdata.n; i++)
            {
                
                if (dcom.GetStr(condition[0].Replace("$id$", i.ToString()).Replace("$arr$","")) == "true")
                {
                    
                    tdata.Cls[i] = color;
                }
            }
        }
        public void AddEdges(bool isarray, List<string> data, Color clr)
        {
            int sz = tdata.n;
            for (int i = 0; i < sz; i++)
            {
                if (isarray)
                {
                    int ssz = dcom.GetNum(data[0].Replace("$id$", i.ToString()).Split(seps,StringSplitOptions.None)[0] + "size()");
                    for (int j = 0; j < ssz; j++)
                    {
                        tdata.Edges[i].Add(dcom.GetNum(data[0].Replace("$id$", i.ToString()).Replace("$arr$","").Replace("$pos$",j.ToString())));
                    }
                }
                else
                {
                    tdata.Edges[i].Add(dcom.GetNum(data[0].Replace("$id$", i.ToString()).Replace("$arr$","")));
                }
            }
        }
        public TreeDataFactoryMain()
        {

        }

    }

    public class TreeDataFactoryStruct:TreeDataFactory
    {
        TreeDataMain tdata = new TreeDataMain();
        DebComm dcom;
        string globalTypename;
        Dictionary<int, string> definitions = new Dictionary<int, string>();
        Dictionary<string, int> revdefinitions = new Dictionary<string, int>();
        public TreeDataFactoryStruct(DebComm dcom)
        {
            this.dcom = dcom;
        }
        public TreeDataMain getData()
        {

            return tdata;
        }
        int ctr = 0;
        string[] seps = new string[1] { "$arr$" };

        void dfs(List<KeyValuePair<bool, List<string>>> data,string root,string typename)
        {
            string rstr = dcom.GetStr(root);
            if (dcom.GetStr(root).Split()[0]== "0x0000000000000000"||dcom.GetStr(root).Split()[0]=="0x00000000")
            {
                return;
            }
            int pos = ctr;
            revdefinitions.Add(root, ctr);
            definitions.Add(ctr++,root);
           
            tdata.Cls.Add(Colors.White);
            tdata.Data.Add(new List<KeyValuePair<string, string>>());
            tdata.G.Add(new List<int>());
            tdata.PG.Add(new List<int>());
            string ptr = dcom.GetPtr(root, typename);
            bool ok = false;
            foreach (var par in data)
            {
                if (!ok && tdata.isshifted == 2)
                {
                    if (par.Key == true)
                    {
                        //int ssz = dcom.GetNum(par.Value[0].Replace("$id$",ptr).Split(seps,StringSplitOptions.None)[0] + ".size()");
                        for (int j = dcom.GetNum(par.Value[0].Replace("$id$",ptr)); (dcom.GetStr(par.Value[1].Replace("$id$", ptr).Replace("$pos$", j.ToString())) == "true"); j = dcom.GetNum(par.Value[2].Replace("$id$", ptr).Replace("$pos$", j.ToString())))
                        {
                            string cur = dcom.GetStr(par.Value[3].Replace("$id$", ptr).Replace("$arr$","").Replace("$pos$",j.ToString())).Split()[0];
                            if (cur != "0x0000000000000000" && cur != "0x00000000")
                            {
                                tdata.PG[pos].Add(ctr);
                                dfs(data, cur, typename);
                            }
                        }

                    }
                    else
                    {
                        string request = par.Value[0].Replace("$id$", ptr).Replace("$arr$","");
                        string cur = dcom.GetStr(par.Value[0].Replace("$id$", ptr).Replace("$arr$","")).Split()[0];

                        if (cur != "0x0000000000000000" && cur != "0x00000000")
                        {
                            tdata.PG[pos].Add(ctr);
                            dfs(data, cur, typename);
                        }
                    }
                }
                else
                {
                    if (par.Key == true)
                    {
                        int ssz = dcom.GetNum(par.Value[0].Replace("$id$", ptr).Split(seps,StringSplitOptions.None)[0] + ".size()");
                        for (int j = dcom.GetNum(par.Value[0].Replace("$id$",ptr)); (dcom.GetStr(par.Value[1].Replace("$id$", ptr).Replace("$pos$", j.ToString())) == "true"); j = dcom.GetNum(par.Value[2].Replace("$id$",ptr).Replace("$pos$", j.ToString())))
                        {
                            string cur = dcom.GetStr(par.Value[3].Replace("$id$", ptr).Replace("$arr$","").Replace("$pos$",j.ToString())).Split()[0];
                            if (cur != "0x0000000000000000" && cur != "0x00000000")
                            {
                                tdata.G[pos].Add(ctr);
                                dfs(data, cur, typename);
                            }
                        }

                    }
                    else
                    {
                        string cur = dcom.GetStr(par.Value[0].Replace("$id$", ptr).Replace("$arr$","")).Split()[0];

                        if (cur != "0x0000000000000000" && cur != "0x00000000")
                        {
                            tdata.G[pos].Add(ctr);
                            dfs(data, cur, typename);
                        }
                    }
                }
                ok = true;
            }
        }
        public void AddMainTree(List<KeyValuePair<bool, List<string>>> data, string root,string typename,int type)
        {
            tdata.isshifted = type;
            

            globalTypename = typename;
            dfs(data, root, typename);
            tdata.n = ctr;
            tdata.Update();
        }
        public void AddInfo(string label, List<string> info)
        {
            string typename = globalTypename;
            int n = tdata.n;
            for (int i = 0; i < n; i++)
            {
                string req = info[0].Replace("$id$", dcom.GetPtr(definitions[i], typename)).Replace("$arr$","");


                string ans = dcom.GetStr(info[0].Replace("$id$",dcom.GetPtr(definitions[i], typename)).Replace("$arr$",""));
                if (ans == "{...}") {
                    ans = "";
                    int szz = dcom.GetNum(req + ".size()");
                    for (int j = 0; j < szz; j++)
                    {
                        ans += dcom.GetStr(req + "[" + j.ToString() + "]")[1];
                    }
                }
                tdata.Data[i].Add(new KeyValuePair<string, string>(label, ans))  ;
            }
        }
        public void AddColor(List<string> condition, Color color)
        {
            string typename = globalTypename;
            for (int i = 0; i < tdata.n; i++)
            {

                if (dcom.GetStr(condition[0].Replace("$id$",dcom.GetPtr(definitions[i], typename))).Replace("$arr","") == "true")
                {

                    tdata.Cls[i] = color;
                }
            }
        }
        public void AddEdges(bool isarray, List<string> data, Color clr)
        {
            string typename = globalTypename;
            int sz = tdata.n;
            for (int i = 0; i < sz; i++)
            {
                if (isarray)
                {
                    int ssz = dcom.GetNum(data[0] + i.ToString() + data[1] + "size()");
                    for (int j = 0; j < ssz; j++)
                    {
                        string cur = dcom.GetStr(data[0] + dcom.GetPtr(definitions[i], typename) + data[1] + "[" + j.ToString() + "]" + data[2]).Split()[0];
                        if (cur != "0x0000000000000000"&&cur!="0x00000000") {
                            tdata.Edges[i].Add(revdefinitions[cur]);
                        }
                    }
                }
                else
                {
                    string cur = dcom.GetStr(data[0] + dcom.GetPtr(definitions[i], typename) + data[1]).Split()[0];
                    if (cur != "0x0000000000000000"&&cur!="0x00000000")
                    {
                        tdata.Edges[i].Add(revdefinitions[cur]);
                    }
                }
            }
        }
        public TreeDataFactoryStruct()
        {

        }

    }


    public class TreeDataMain : TreeData

    {
        delegate void DFS(int v, int prev);
        DFS dfs;
       void dfsnotshifted(int v,int prev)
        {
            vis[v] = 1;
            if (prev == -1)
            {
                Level[v] = 0;

            }
            else
            {
                Level[v] = Level[prev] + 1;
            }
            maxdep = Math.Max(maxdep, Level[v]);
            Tin[v] = ctr;
            bool ok = false;
            foreach (int h in G[v])
            {

                if (vis[h] == 0)
                {
                    if (ok)
                    {
                        ctr++;
                    }
                    ok = true;
                    dfs(h, v);
                }
            }
            Tout[v] = ctr;
        }
        void dfsshifted(int v, int prev)
        {
            vis[v] = 1;
            if (prev == -1)
            {
                Level[v] = 0;

            }
            else
            {
                Level[v] = Level[prev] + 1;
            }
            maxdep = Math.Max(maxdep, Level[v]);
            Tin[v] = ++ctr;
            bool ok = false;
            foreach (int h in G[v])
            {

                if (vis[h] == 0)
                {
                    
                    dfs(h, v);
                }
            }
            Tout[v] = ctr;

        }
        void dfsspec(int v, int prev)
        {
            vis[v] = 1;
            if (prev == -1)
            {
                Level[v] = 0;

            }
            else
            {
                Level[v] = Level[prev] + 1;
            }
            maxdep = Math.Max(maxdep, Level[v]);
            
            bool ok = false;
            foreach (int h in PG[v])
            {

                if (vis[h] == 0)
                {

                    dfs(h, v);
                }
            }
            Tin[v] = Tout[v] = ctr++;
            foreach (int h in G[v])
            {

                if (vis[h] == 0)
                {

                    dfs(h, v);
                }
            }

        }
        public void Update()
        {
            if (isshifted==1)
            {
                dfs = dfsshifted;
                ctr = -1;
            }
            else
            {
                if (isshifted == 0)
                {
                    dfs = dfsnotshifted;
                    ctr = 0;
                }
                else
                {
                    dfs = dfsspec;
                    ctr = 0;
                }
            }
            for (int i = 0; i < n; i++)
            {
                
                vis.Add(0);
                Tin.Add(-1);
                Level.Add(0);
                Tout.Add(0);
            }
            if (vis.Count>0)
            dfs(root, -1);
            for (int i = 0; i < G.Count; i++)
            {
                G[i].AddRange(PG[i]);
            }
        }

    }










}

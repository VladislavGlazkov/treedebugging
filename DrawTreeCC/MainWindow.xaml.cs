﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TreeData;
namespace DrawTreeCC
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class DrawTreeC : Window,IDrawTree
    {
        TreeData.TreeData lastData;
        public DrawTreeC()
        {
            InitializeComponent();
            this.SizeChanged += DrawTreeC_SizeChanged; ;
        }
        public void SetNum(int num)
        {
            this.Title = "Window " + num.ToString();
        }
        private void DrawTreeC_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Redraw();
        }
        List<Line> Arrow(Line origin)
        {
            Vector pt1 = new Vector(origin.X2 - origin.X1, origin.Y2 - origin.Y1);
            double len = pt1.Length;
            pt1 *= (len - 50) / len;
            Vector pt2 = pt1 * ((len - 70) / (len - 50));
            Vector pttemp = pt1 - pt2;
            Vector pt3 = new Vector(pttemp.Y, -pttemp.X) * 0.5 + pt2;
            Vector pt4 = new Vector(-pttemp.Y, pttemp.X) * 0.5 + pt2;
            Vector basee = new Vector(origin.X1, origin.Y1);
            pt1 += basee;
            pt3 += basee;
            pt4 += basee;
            Line l2 = new Line();
            l2.X1 = pt1.X;
            l2.Y1 = pt1.Y;
            l2.X2 = pt3.X;
            l2.Y2 = pt3.Y;
            l2.Stroke = origin.Stroke.Clone();

            l2.StrokeThickness = origin.StrokeThickness;
            Line l3 = new Line();
            l3.X1 = pt1.X;
            l3.Y1 = pt1.Y;
            l3.X2 = pt4.X;
            l3.Y2 = pt4.Y;
            l3.Stroke = origin.Stroke.Clone();
            l3.StrokeThickness = origin.StrokeThickness;
            return new List<Line> { origin, l2, l3 };


        }
        public void Redraw(TreeData.TreeData data)
        {
            if (data != null)
            {
                lastData = data;
                Canvas canvas = new Canvas();
                int n = data.n;
                double tn = 0;
                double ln = 0;
                for (int i = 0; i < n; i++)
                {
                    tn = Math.Max(tn, data.Tout[i]);
                    ln = Math.Max(ln, data.Level[i]);
                }
                double width = this.Width;
                double height = this.Height;
                List<Vector> coords = new List<Vector>();
                for (int i = 0; i < n; i++)
                {
                    double tmid = ((double)(data.Tin[i] + data.Tout[i])) / 2;
                    coords.Add( new Vector(100 + (tmid / (tn + 1)) * (width - 100), 100 + (height - 100) * (data.Level[i] / (ln + 1))));


                }
                for (int i = 0; i < n; i++)
                {
                    if (data.Tin[i] >= 0)
                    {
                        foreach (int h in data.G[i])
                        {
                            Line l = new Line();
                            l.X1 = coords[i].X;
                            l.Y1 = coords[i].Y;
                            l.X2 = coords[h].X;
                            l.Y2 = coords[h].Y;
                            l.StrokeThickness = 2;
                            l.Stroke = new SolidColorBrush(Colors.Black);

                            foreach (Line line in Arrow(l))
                            {
                                Canvas.SetZIndex(line, 0);
                                canvas.Children.Add(line);
                            }
                        }
                        Ellipse elp = new Ellipse();
                        elp.Width = 100;
                        elp.Height = 100;
                        elp.Stroke = new SolidColorBrush(Colors.Black);
                        elp.StrokeThickness = 3;
                        elp.Fill = new SolidColorBrush(data.Cls[i]);

                        Canvas.SetZIndex(elp, 1);
                        Canvas.SetLeft(elp, coords[i].X - 50);
                        Canvas.SetTop(elp, coords[i].Y - 50);
                        canvas.Children.Add(elp);
                        Grid stackPanel = new Grid();
                        stackPanel.Width = 100;
                        stackPanel.Height = 100;
                        stackPanel.ColumnDefinitions.Add(new ColumnDefinition());
                        stackPanel.RowDefinitions.Add(new RowDefinition());

                        TextBlock tbx = new TextBlock();
                        tbx.Text = "ID: " + i.ToString();
                        foreach (var par in data.Data[i])
                        {
                            tbx.Text += "\n" + par.Key + ": " + par.Value;
                        }
                        tbx.HorizontalAlignment = HorizontalAlignment.Center;
                        tbx.VerticalAlignment = VerticalAlignment.Center;
                        Grid.SetColumn(tbx, 0);
                        Grid.SetRow(tbx, 0);
                        stackPanel.Children.Add(tbx);

                        Canvas.SetZIndex(stackPanel, 2);
                        Canvas.SetLeft(stackPanel, coords[i].X - 50);
                        Canvas.SetTop(stackPanel, coords[i].Y - 50);
                        canvas.Children.Add(stackPanel);

                        this.Content = canvas;
                    }
                }
            }
        }
        void Redraw()
        {
            Redraw(lastData);
        }
    }
}

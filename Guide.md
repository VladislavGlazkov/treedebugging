﻿Installation guide:

1. Install the latest version of Visual Studio.
1. Double-click the provided .vsix file. 
1. Follow the installer instructions.

How to use:

1. Open C++ Visual Studio project
1. Go to View -> ToolWindow 1
1. Set up all the necessary parameters (as described below)
1. Start the program with the local Visual Studio debugger.
1. When the first breakpoint is reached, the visualization windows will appear
1. When the program execution is terminated, the visualization windows will disappear.

Settings:

File panel:

- Save. Is used to save current parameters to a file.
- Open. Is used to restore all the parameters from a file (attention: use only the files, created with the Save option)

Window Tabs:

Use the + button to add different windows. Each window has its own set of parameters and is visualized independently. Make a double-click on an existing window to delete it.

Data mode:

1. Use Standard in case a unique identifier for a vertex is an index (for example, if the tree is stored as several arrays or as an array of structs). If you’re using this mode, you need to fill the first text field with the index of the root vertex, the second one – with the maximal index of a vertex, increased by 1.

1. Use Struct in case a unique identifier for a vertex is a pointer (for example, if the tree is defined with a root vertex and pointers to the child vertices). If you’re using this mode, you should fill the first text field with the pointer to the root vertex, the second one – with the type name of vertex pointers (for example, “node” in case the type of pointers is “node\*”)

Views:

1. Use Standard View to have the tree visualized with the circles for vertices and arrow connections between them.
1. Use Rectangle View to have the tree visualized with the rectangles for vertices. Each pair of connected vertices borders by a segment.

Display modes:

1. Standard. Recommended for general use. In rectangle mode, a parent vertex will be stretched to the total size of all the child ones. 
1. Shifted. In rectangle mode, the first and the last sector each vertex covers stand for input and output time.
1. Treap. Use it with Standard view ONLY. Vertices from the first Tree Edges line will be displayed to the left from the parent one, all the other vertices – to the right. Preferred to visualize treap.

Remark: for Standard and Shifted views in the Standard mode, the center of a vertex will be set to the center of the appropriate rectangle in Rectangle mode.

`	`Tree Edges:

Here you can set, which vertices is each vertex connected to. Press add to create new lines. Make a double-click on a line to delete it. The way of filing the fields is described below. Use “$id$” for the vertex index in Standard data mode and for the pointer in Struct data mode, $pos$ - in Array mode for the identifier of the current position.

1. Single. The only field should be filled with the expression for the vertex connected to the current one in terms of $id$.
1. Array. The first field should be filled with the initial value of $pos$, the second one – with the condition (must be of “bool” type) for continuing iteration, the third one – with the new value of $pos$ on each step, the fourth one – with the expression for the vertex connected to the current one in terms of $id$ and $pos$

Data Picker:

Here you can set, which data will be displayed inside each vertex.

First field should be filled with the label for this line, second one – with the data stored (formats: int, bool, char, string) in terms of $id$.

Color Vertices

Here you can fill any vertex with some color.

First field stands for the expression (bool format) in terms of $id$ for applying the appropriate color assignment in case the value is true, the second one is the color for the assignment.

Remark: if a vertex has several true values, only the last one will be applied.

